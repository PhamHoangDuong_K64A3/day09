<?php
$questions = array(
    "1" => array(
        "question" => "What does PHP used for?",
        "choices" => array(
            "a" => "Web development",
            "b" => "Desktop applications",
            "c" => "Both of the above",
            "d" => "None of the above"
        )
    ),
    "2" => array(
        "question" => "What is the correct way to end a PHP statement?",
        "choices" => array("a" => "New line", "b" => "Semicolon", "c" => "Both", "d" => "None")
    ),
    "3" => array(
        "question" => "Which of the following is not a valid function in PHP?",
        "choices" => array("a" => "is_int()", "b" => "is_integer()", "c" => "is_long()", "d" => "is_real()")
    ),
    "4" => array(
        "question" => "Which of the following is not a valid constant in PHP?",
        "choices" => array("a" => "PHP_VERSION", "b" => "PHP_OS", "c" => "PHP_DEBUG", "d" => "PHP_ERROR")
    ),
    "5" => array(
        "question" => "Which of the following is not a valid operator in PHP?",
        "choices" => array("a" => "and", "b" => "or", "c" => "xor", "d" => "&&")
    )
);

const ANSWER = array(
    "1" => "a",
    "2" => "b",
    "3" => "d",
    "4" => "d",
    "5" => "a"
);

if (isset($_POST['submit'])) {
    $score = 0;
    for ($i = 1; $i <= 5; $i++) {
        if ($_POST[$i] == ANSWER[$i]) {
            $score++;
        }
    }
    setcookie("score", $score, time() + (86400 * 30), "/");
    header("Location: page2.php");
    exit();
}

?>

<html>

<head>
    <meta charset="utf-8">
    <title>day09 - page 1</title>
</head>

<body>
    <div>
        <form action="page1.php" method="post">
            <?php
            foreach ($questions as $id => $question) {
                echo "
                    <h3>" . $id . ". " . $question['question'] . "</h3>
                    <div>
                        <input type='radio' name='" . $id . "' id='a' value='a' />
                        <label for='a'>" . $question['choices']['a'] . "</label>
                    </div>
                    <div>
                        <input type='radio' name='" . $id . "' id='b' value='b' />
                        <label for='b'>" . $question['choices']['b'] . "</label>
                    </div>
                    <div>
                        <input type='radio' name='" . $id . "' id='c' value='c' />
                        <label for='c'>" . $question['choices']['c'] . "</label>
                    </div>
                    <div>
                        <input type='radio' name='" . $id . "' id='d' value='d' />
                        <label for='d'>" . $question['choices']['d'] . "</label>
                    </div>";
            }
            ?>
            <input type="submit" name="submit" value="Next" />
        </form>
    </div>

</body>

</html>
