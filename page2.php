<?php
$questions = array(
    "6" => array(
        "question" => "Which PHP function is used to get the current Unix timestamp?",
        "choices" => array(
            "a" => "time()",
            "b" => "getdate()",
            "c" => "localtime()",
            "d" => "mktime()"
        )
    ),
    "7" => array(
        "question" => "What is the correct way to start a session in PHP?",
        "choices" => array("a" => "session_start()", "b" => "session()", "c" => "start_session()", "d" => "start()"),
    ),
    "8" => array(
        "question" => "How do you echo text in PHP?",
        "choices" => array("a" => "echo", "b" => "print", "c" => "print()", "d" => "echo()")
    ),
    "9" => array(
        "question" => "How many types of errors does PHP support?",
        "choices" => array("a" => "3", "b" => "4", "c" => "5", "d" => "6")
    ),
    "10" => array(
        "question" => "How do you get the length of a string in PHP?",
        "choices" => array("a" => "strlen()", "b" => "length()", "c" => "size()", "d" => "count()")
    )
);

const ANSWER = array(
    "6" => "a",
    "7" => "a",
    "8" => "a",
    "9" => "b",
    "10" => "a"
);

if (isset($_POST['submit'])) {
    $score = $_COOKIE['score'];
    for ($i = 6; $i <= 10; $i++) {
        if ($_POST[$i] == ANSWER[$i]) {
            $score++;
        }
    }
    setcookie("score", $score, time() + (86400 * 30), "/");
    header("Location: page3.php");
    exit();
}

?>

<html>

<head>
    <meta charset="utf-8">
    <title>day09 - page 2</title>
</head>

<body>
    <div>
        <form action="page2.php" method="post">
            <?php
            foreach ($questions as $id => $question) {
                echo "
                    <h3>" . $id . ". " . $question['question'] . "</h3>
                    <div>
                        <input type='radio' name='" . $id . "' id='a' value='a' />
                        <label for='a'>" . $question['choices']['a'] . "</label>
                    </div>
                    <div>
                        <input type='radio' name='" . $id . "' id='b' value='b' />
                        <label for='b'>" . $question['choices']['b'] . "</label>
                    </div>
                    <div>
                        <input type='radio' name='" . $id . "' id='c' value='c' />
                        <label for='c'>" . $question['choices']['c'] . "</label>
                    </div>
                    <div>
                        <input type='radio' name='" . $id . "' id='d' value='d' />
                        <label for='d'>" . $question['choices']['d'] . "</label>
                    </div>
                ";
            }
            ?>
            <input type="submit" name="submit" value="Nộp bài" />
        </form>
    </div>

</body>

</html>
